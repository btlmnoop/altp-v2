/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

public class Option {

    public String content;
    public boolean isAnswer;
    
    public  Option(){
        content="";
        isAnswer=false;
    }
    
    public Option(String content, boolean isAnswer){
        this.content=content;
        this.isAnswer=isAnswer;
    }
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    public boolean checkAnswer(){
        if (isAnswer) {
            return true;
        }
        return false;
    }
}
