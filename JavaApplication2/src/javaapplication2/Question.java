/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

public class Question {

    String content;
    Option[] option;
    final int NUMBER_OF_OPTION = 4;
    String answer;

    public Question() {
        content = "Chưa có nội dung câu hỏi";
        option = new Option[NUMBER_OF_OPTION];
    }

    public Question(String content, String op1, String op2, String op3, String op4, String answer) {
        this.content = content;
        option = new Option[NUMBER_OF_OPTION];
        option[0] = new Option(op1, false);
        option[1] = new Option(op2, false);
        option[2] = new Option(op3, false);
        option[3] = new Option(op4, false);
        this.answer = answer;
    }

    String getContent() {
        return content;
    }

    String getOption(int i) {
        return option[i].getContent();
    }

    String getAnswer() {
        return answer;
    }
}
