/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.util.Timer;
import java.util.TimerTask;

public class NewJPanel extends javax.swing.JPanel {

    /**
     * Creates new form NewJPanel
     */
    Question[] q = new Question[30];
    int i = 0;
    int money = 0;
    ImageIcon icon;
    Image img;
    Image newimg;
    int sec = 0;
    int minute = 0;
    int timeout = -1;

    public NewJPanel() {
        initComponents();
        icon = SetIcon("logo.png", 250, 200);
        logoImage1.setIcon(icon);
        icon = SetIcon("logo.png", 120, 96);
        logoImage.setIcon(icon);
        inGameLayer.setVisible(false);

        icon = SetIcon("button.png", 250, 50);
        buttonA.setIcon(icon);
        buttonB.setIcon(icon);
        buttonC.setIcon(icon);
        buttonD.setIcon(icon);

        icon = SetIcon("ask.png", 50, 50);
        askButton.setIcon(icon);

        icon = SetIcon("change.png", 40, 40);
        changeQuestion.setIcon(icon);

        jDialog1.setLocationRelativeTo(this);
        jDialog1.setSize(300, 400);
        jDialog1.setVisible(false);

        try {
            loadQuestion();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NewJPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NewJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void TimeCount() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                count();
                if (minute < 10) {
                    jLabel4.setText("0" + minute);
                } else {
                    jLabel4.setText("" + minute);
                }
                if (sec < 10) {
                    jLabel4.setText(jLabel4.getText() + ":0" + sec);
                } else {
                    jLabel4.setText(jLabel4.getText() + ":" + sec);
                }
            }

            private void count() {
                if (timeout == -1) {
                    if (sec == 0) {
                        if (minute == 0) {
                            timeout = 1;
                            return;
                        }
                        minute--;
                        sec = 59;
                        return;
                    }
                    sec--;
                }
            }
        }, 0, 1000);
        Timer timer1 = new Timer();
        timer1.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (timeout == 1) {
                    if (money==0) {
                        money=1;
                    }
                    JOptionPane.showMessageDialog(null, "Bạn đã hết thời gian trả lời\nTổng số điểm của bạn là " + money, "Kết quả", JOptionPane.INFORMATION_MESSAGE);
                    startLayer.setVisible(true);
                    inGameLayer.setVisible(false);
                    timeout = 0;
                }
            }
        }, 0, 1000);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        icon = new ImageIcon("background.jpg");
        img = icon.getImage();
        g.drawImage(img, 0, 0, 500, 700, this);
    }

    private ImageIcon SetIcon(String dir, int i1, int i2) {
        ImageIcon icon1 = new ImageIcon(dir);
        Image image = icon1.getImage();
        Image newImg = image.getScaledInstance(i1, i2, java.awt.Image.SCALE_SMOOTH);
        icon1 = new ImageIcon(newImg);
        return icon1;
    }

    private void answerButtonClick(String str, ActionEvent e) {
        JButton b = (JButton) e.getSource();
        icon = SetIcon("yellow.png", 250, 50);
        b.setIcon(icon);
        int n = JOptionPane.showConfirmDialog(null, "Bạn có chắc với phương án lựa chọn này?", "Trả lời", JOptionPane.YES_NO_OPTION);
        if (n == JOptionPane.YES_OPTION) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(NewJPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (str.equals(q[i - 1].getAnswer())) {
                icon = SetIcon("green.png", 250, 50);
                b.setIcon(icon);
                JOptionPane.showMessageDialog(null, "Bạn đã trả lời chính xác", "Chính xác", JOptionPane.INFORMATION_MESSAGE);
                updateMoney();
                if (i == 15) {
                    JOptionPane.showMessageDialog(null, "Bạn đã hoàn thành tất cả các câu hỏi\nTổng số điểm của bạn là " + money, "Kết quả", JOptionPane.INFORMATION_MESSAGE);
                    startLayer.setVisible(true);
                    inGameLayer.setVisible(false);
                } else {
                    updateQuestion();
                }
                icon = SetIcon("button.png", 250, 50);
                b.setIcon(icon);
            } else {
                icon = SetIcon("red.png", 250, 50);
                b.setIcon(icon);
                JButton ab = getCorrectAnswer(q[i - 1].getAnswer());
                icon = SetIcon("green.png", 250, 50);
                ab.setIcon(icon);
                if (money==0) {
                    money=1;
                }
                JOptionPane.showMessageDialog(null, "Bạn đã trả lời sai\nTổng số điểm của bạn là " + money, "Kết quả", JOptionPane.INFORMATION_MESSAGE);
                icon = SetIcon("button.png", 250, 50);
                b.setIcon(icon);
                ab.setIcon(icon);
                startLayer.setVisible(true);
                inGameLayer.setVisible(false);
            }
        } else {
            icon = SetIcon("button.png", 250, 50);
            b.setIcon(icon);
        }
    }

    private JButton getCorrectAnswer(String answer) {
        switch (answer) {
            case "A":
                return buttonA;
            case "B":
                return buttonB;
            case "C":
                return buttonC;
            case "D":
                return buttonD;
            default:
                return null;
        }
    }

    private void updateMoney() {
        if (i < 4) {
            money += 1000;
        } else if (i < 5) {
            money += 2000;
        } else if (i < 6) {
            money += 5000;
        } else if (i < 8) {
            money += 10000;
        } else if (i < 10) {
            money += 30000;
        } else if (i < 11) {
            money += 60000;
        } else if (i < 13) {
            money += 100000;
        } else if (i < 14) {
            money += 150000;
        } else if (i < 15) {
            money += 300000;
        } else {
            money += 400000;
        }
        moneyLabel.setText("$" + money);
    }

    private void updateQuestion() {
        question.setText("<html>Câu hỏi " + (i + 1) + " : " + q[i].getContent() + "</html>");
        buttonA.setText("A. " + q[i].getOption(0));
        buttonB.setText("B. " + q[i].getOption(1));
        buttonC.setText("C. " + q[i].getOption(2));
        buttonD.setText("D. " + q[i].getOption(3));
        i++;
    }

    private void loadQuestion() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        InputStream is = new FileInputStream("data1.txt");
        String UTF8 = "utf8";

        try (BufferedReader br = new BufferedReader(new InputStreamReader(is, UTF8))) {
            String str;
            String[] b;
            int count = 0;
            while ((str = br.readLine()) != null) {
                b = str.split("-");
                q[count++] = new Question(b[1], b[2], b[3], b[4], b[5], b[6]);
            }
        }
        is.close();
    }

    private Question[] Shuffle(Question[] q) {
        int length = q.length;
        Random rand = new Random();
        int r;
        for (int j = 0; j < length; j++) {
            r = rand.nextInt(length - 1);
            Question tmp;
            tmp = q[j];
            q[j] = q[r];
            q[r] = tmp;
        }
        return q;
    }

    private void RandomOption(JButton x, JButton y, JButton z) {
        Random rand = new Random();
        int n = rand.nextInt(3) + 1;
        switch (n) {
            case 1:
                y.setVisible(false);
                z.setVisible(false);
                break;
            case 2:
                x.setVisible(false);
                z.setVisible(false);
                break;
            case 3:
                x.setVisible(false);
                y.setVisible(false);
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        aPercent = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        aLabel = new javax.swing.JLabel();
        bLabel = new javax.swing.JLabel();
        cLabel = new javax.swing.JLabel();
        dLabel = new javax.swing.JLabel();
        bPercent = new javax.swing.JLabel();
        cPercent = new javax.swing.JLabel();
        dPercent = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        startLayer = new javax.swing.JLayeredPane();
        logoImage1 = new javax.swing.JLabel();
        startButton = new javax.swing.JButton();
        inGameLayer = new javax.swing.JLayeredPane();
        buttonD = new javax.swing.JButton();
        buttonC = new javax.swing.JButton();
        buttonB = new javax.swing.JButton();
        buttonA = new javax.swing.JButton();
        question = new javax.swing.JLabel();
        logoImage = new javax.swing.JLabel();
        moneyLabel = new javax.swing.JLabel();
        askButton = new javax.swing.JButton();
        changeQuestion = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        jDialog1.setTitle("Trợ giúp");
        jDialog1.setAlwaysOnTop(true);
        jDialog1.setModal(true);
        jDialog1.setResizable(false);
        jDialog1.setSize(new java.awt.Dimension(300, 200));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Tỉ lệ khán giả lựa chọn");

        aLabel.setText("A");

        bLabel.setText("B");

        cLabel.setText("C");

        dLabel.setText("D");

        jLabel7.setText("jLabel7");

        jLabel8.setText("jLabel8");

        jLabel9.setText("jLabel9");

        jLabel10.setText("jLabel10");

        jButton1.setText("Đóng");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jDialog1Layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(bLabel)
                                    .addComponent(aLabel)
                                    .addComponent(cLabel)
                                    .addComponent(dLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                                        .addComponent(dPercent, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(bPercent, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cPercent, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(aPercent, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addComponent(jButton1)))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jDialog1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {aPercent, bPercent, cPercent, dPercent});

        jDialog1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel10, jLabel7, jLabel8, jLabel9});

        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(aPercent, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bPercent, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cPercent, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dPercent, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addComponent(aLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                        .addGap(11, 11, 11)
                        .addComponent(bLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addGap(0, 73, Short.MAX_VALUE))
        );

        jDialog1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {aLabel, aPercent, bLabel, bPercent, cLabel, cPercent, dLabel, dPercent, jLabel10, jLabel7, jLabel8, jLabel9});

        setPreferredSize(new java.awt.Dimension(500, 700));

        logoImage1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoImage1.setAlignmentY(0.0F);
        logoImage1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        startButton.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        startButton.setText("Bắt Đầu");
        startButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        startLayer.setLayer(logoImage1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        startLayer.setLayer(startButton, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout startLayerLayout = new javax.swing.GroupLayout(startLayer);
        startLayer.setLayout(startLayerLayout);
        startLayerLayout.setHorizontalGroup(
            startLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, startLayerLayout.createSequentialGroup()
                .addContainerGap(55, Short.MAX_VALUE)
                .addComponent(logoImage1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54))
            .addGroup(startLayerLayout.createSequentialGroup()
                .addGap(129, 129, 129)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        startLayerLayout.setVerticalGroup(
            startLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(startLayerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(logoImage1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonD.setForeground(new java.awt.Color(255, 255, 255));
        buttonD.setText("a");
        buttonD.setBorder(null);
        buttonD.setBorderPainted(false);
        buttonD.setContentAreaFilled(false);
        buttonD.setFocusPainted(false);
        buttonD.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDActionPerformed(evt);
            }
        });

        buttonC.setForeground(new java.awt.Color(255, 255, 255));
        buttonC.setText("a");
        buttonC.setBorder(null);
        buttonC.setBorderPainted(false);
        buttonC.setContentAreaFilled(false);
        buttonC.setFocusPainted(false);
        buttonC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCActionPerformed(evt);
            }
        });

        buttonB.setForeground(new java.awt.Color(255, 255, 255));
        buttonB.setText("a");
        buttonB.setBorder(null);
        buttonB.setBorderPainted(false);
        buttonB.setContentAreaFilled(false);
        buttonB.setFocusPainted(false);
        buttonB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBActionPerformed(evt);
            }
        });

        buttonA.setForeground(new java.awt.Color(255, 255, 255));
        buttonA.setText("a");
        buttonA.setBorder(null);
        buttonA.setBorderPainted(false);
        buttonA.setContentAreaFilled(false);
        buttonA.setFocusPainted(false);
        buttonA.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAActionPerformed(evt);
            }
        });

        question.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        question.setText("jLabel1");
        question.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        logoImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoImage.setAlignmentY(0.0F);
        logoImage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        moneyLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        moneyLabel.setForeground(new java.awt.Color(255, 255, 0));
        moneyLabel.setText("$0");

        askButton.setBorder(null);
        askButton.setBorderPainted(false);
        askButton.setContentAreaFilled(false);
        askButton.setFocusPainted(false);
        askButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                askButtonActionPerformed(evt);
            }
        });

        changeQuestion.setBorder(null);
        changeQuestion.setBorderPainted(false);
        changeQuestion.setContentAreaFilled(false);
        changeQuestion.setFocusPainted(false);
        changeQuestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeQuestionActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("jLabel4");

        inGameLayer.setLayer(buttonD, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(buttonC, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(buttonB, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(buttonA, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(question, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(logoImage, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(moneyLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(askButton, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(changeQuestion, javax.swing.JLayeredPane.DEFAULT_LAYER);
        inGameLayer.setLayer(jLabel4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout inGameLayerLayout = new javax.swing.GroupLayout(inGameLayer);
        inGameLayer.setLayout(inGameLayerLayout);
        inGameLayerLayout.setHorizontalGroup(
            inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inGameLayerLayout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addGroup(inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inGameLayerLayout.createSequentialGroup()
                        .addComponent(question, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inGameLayerLayout.createSequentialGroup()
                        .addGroup(inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(inGameLayerLayout.createSequentialGroup()
                                .addComponent(changeQuestion, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(askButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(inGameLayerLayout.createSequentialGroup()
                                .addComponent(moneyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(logoImage, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(74, 74, 74)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42))))
            .addGroup(inGameLayerLayout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addGroup(inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(buttonA, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonB)
                    .addComponent(buttonC)
                    .addComponent(buttonD))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        inGameLayerLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonA, buttonB, buttonC, buttonD});

        inGameLayerLayout.setVerticalGroup(
            inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inGameLayerLayout.createSequentialGroup()
                .addGroup(inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(logoImage, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(inGameLayerLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(moneyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(26, 26, 26)
                .addGroup(inGameLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(changeQuestion, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(askButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(question, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonA, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonB)
                .addGap(18, 18, 18)
                .addComponent(buttonC)
                .addGap(18, 18, 18)
                .addComponent(buttonD)
                .addContainerGap(91, Short.MAX_VALUE))
        );

        inGameLayerLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonA, buttonB, buttonC, buttonD});

        inGameLayerLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel4, moneyLabel});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 45, Short.MAX_VALUE)
                .addComponent(startLayer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 46, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(inGameLayer, javax.swing.GroupLayout.Alignment.TRAILING))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 131, Short.MAX_VALUE)
                .addComponent(startLayer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 131, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(inGameLayer, javax.swing.GroupLayout.Alignment.TRAILING))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        // TODO add your handling code here:
        inGameLayer.setVisible(true);
        startLayer.setVisible(false);
        icon = SetIcon("change.png", 50, 50);
        changeQuestion.setIcon(icon);
        changeQuestion.setEnabled(true);
        icon = SetIcon("ask.png", 50, 50);
        askButton.setIcon(icon);
        askButton.setEnabled(true);

        i = 0;
        money = 0;
        moneyLabel.setText("$"+money);
        q = Shuffle(q);
        updateQuestion();
        sec = 59;
        minute = 9;
        timeout = -1;
        TimeCount();
    }//GEN-LAST:event_startButtonActionPerformed

    private void buttonAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAActionPerformed
        // TODO add your handling code here:
        answerButtonClick("A", evt);
    }//GEN-LAST:event_buttonAActionPerformed

    private void buttonBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBActionPerformed
        // TODO add your handling code here:
        answerButtonClick("B", evt);
    }//GEN-LAST:event_buttonBActionPerformed

    private void buttonCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCActionPerformed
        // TODO add your handling code here:
        answerButtonClick("C", evt);
    }//GEN-LAST:event_buttonCActionPerformed

    private void buttonDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDActionPerformed
        // TODO add your handling code here:
        answerButtonClick("D", evt);
    }//GEN-LAST:event_buttonDActionPerformed

    private void changeQuestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeQuestionActionPerformed
        // TODO add your handling code here:
        int j;
        j = JOptionPane.showConfirmDialog(null, "Bạn cần sự trợ giúp từ khán giả", "Trợ giúp", JOptionPane.YES_NO_OPTION);
        if (j == JOptionPane.NO_OPTION) {
            return;
        }
        icon = SetIcon("lock.png", 50, 50);
        changeQuestion.setIcon(icon);
        changeQuestion.setEnabled(false);
        i--;
        q[i] = q[25];
        updateQuestion();
    }//GEN-LAST:event_changeQuestionActionPerformed

    private void askButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_askButtonActionPerformed
        // TODO add your handling code here:
        int j;
        j = JOptionPane.showConfirmDialog(null, "Bạn cần sự trợ giúp từ khán giả", "Trợ giúp", JOptionPane.YES_NO_OPTION);
        if (j == JOptionPane.NO_OPTION) {
            return;
        }
        icon = SetIcon("lock.png", 50, 50);
        askButton.setIcon(icon);
        askButton.setEnabled(false);

        int n;
        Random rand = new Random();
        if (i < 6) {
            n = rand.nextInt(70) + 1;
        } else if (i < 11) {
            n = rand.nextInt(90) + 1;
        } else {
            n = rand.nextInt(100) + 1;
        }

        float width = (100 - n) * 167 / 100;
        icon = SetIcon("redoption.png", (int) width, 46);
        switch (q[i - 1].getAnswer()) {
            case "A":
                aPercent.setIcon(icon);
                jLabel7.setText("" + (100 - n) + "%");
                optionPercent(bPercent, jLabel8, cPercent, jLabel9, dPercent, jLabel10, n);
                break;
            case "B":
                bPercent.setIcon(icon);
                jLabel8.setText("" + (100 - n) + "%");
                optionPercent(cPercent, jLabel9, dPercent, jLabel10, aPercent, jLabel7, n);
                break;
            case "C":
                cPercent.setIcon(icon);
                jLabel9.setText("" + (100 - n) + "%");
                optionPercent(dPercent, jLabel10, aPercent, jLabel7, bPercent, jLabel8, n);
                break;
            default:
                dPercent.setIcon(icon);
                jLabel10.setText("" + (100 - n) + "%");
                optionPercent(aPercent, jLabel7, cPercent, jLabel9, bPercent, jLabel8, n);
                break;
        }
        jDialog1.setVisible(true);
    }//GEN-LAST:event_askButtonActionPerformed

    private void optionPercent(JLabel x, JLabel o, JLabel y, JLabel p, JLabel z, JLabel q, int n) {
        Random rand = new Random();
        int m;
        m = rand.nextInt(n) + 1;
        float width = m * 167 / 100;
        icon = SetIcon("redoption.png", (int) width, 46);
        x.setIcon(icon);
        o.setText("" + m + "%");
        n = n - m;
        if (n == 0) {
            icon = SetIcon("redoption.png", 1, 46);
            y.setIcon(icon);
            p.setText("" + 0 + "%");
            z.setIcon(icon);
            q.setText("" + 0 + "%");
            return;
        }
        m = rand.nextInt(n) + 1;
        width = m * 167 / 100;
        icon = SetIcon("redoption.png", (int) width, 46);
        y.setIcon(icon);
        p.setText("" + m + "%");
        n = n - m;
        if (n == 0) {
            icon = SetIcon("redoption.png", 1, 46);
            z.setIcon(icon);
            q.setText("" + 0 + "%");
            return;
        }
        width = n * 167 / 100;
        icon = SetIcon("redoption.png", (int) width, 46);
        z.setIcon(icon);
        q.setText("" + n + "%");
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jDialog1.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel aLabel;
    private javax.swing.JLabel aPercent;
    private javax.swing.JButton askButton;
    private javax.swing.JLabel bLabel;
    private javax.swing.JLabel bPercent;
    private javax.swing.JButton buttonA;
    private javax.swing.JButton buttonB;
    private javax.swing.JButton buttonC;
    private javax.swing.JButton buttonD;
    private javax.swing.JLabel cLabel;
    private javax.swing.JLabel cPercent;
    private javax.swing.JButton changeQuestion;
    private javax.swing.JLabel dLabel;
    private javax.swing.JLabel dPercent;
    private javax.swing.JLayeredPane inGameLayer;
    private javax.swing.JButton jButton1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel logoImage;
    private javax.swing.JLabel logoImage1;
    private javax.swing.JLabel moneyLabel;
    private javax.swing.JLabel question;
    private javax.swing.JButton startButton;
    private javax.swing.JLayeredPane startLayer;
    // End of variables declaration//GEN-END:variables
}
